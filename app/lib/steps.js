import { me } from 'appbit';
import { today } from 'user-activity';
import { display } from 'display';

const CALLBACK_BUFFER = 1000; // 1s

export function init(callback) {
    if (me.permissions.granted('access_activity')) {
        let interval;

        const start = () => {
            interval = setInterval(() => callback({steps: today.adjusted.steps}), CALLBACK_BUFFER);
        };
        const stop = () => clearInterval(interval);

        display.addEventListener('charge', () => {
            if (display.on) {
                start();
            } else {
                stop();
            }
        });

        start();
    } else {
        callback({steps: '???'});
    }
}
