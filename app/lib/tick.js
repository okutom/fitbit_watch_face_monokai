import clock from 'clock';
import { battery } from 'power';
import { today as activityToday } from 'user-activity';
import { preferences } from 'user-settings';
import * as util from './utils.js';

let tickCallback;
let dayArray = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

export function init(granularity, callback) {
    clock.granularity = granularity;
    tickCallback = callback;
    clock.addEventListener('tick', tickHandler);
}

function tickHandler(evt) {
    let batLevel = Math.floor(battery.chargeLevel);
    let batLevelString = batLevel + '%';
    let batIconString = '';
    if(battery.charging) {
        batIconString = '';
    } else if(batLevel > 95) {
        batIconString = '';
    } else if(batLevel >= 85) {
        batIconString = '';
    } else if(batLevel >= 75) {
        batIconString = '';
    } else if(batLevel >= 65) {
        batIconString = '';
    } else if(batLevel >= 55) {
        batIconString = '';
    } else if(batLevel >= 45) {
        batIconString = '';
    } else if(batLevel >= 35) {
        batIconString = '';
    } else if(batLevel >= 25) {
        batIconString = '';
    } else if(batLevel >= 15) {
        batIconString = '';
    } else if(batLevel >= 10) {
        batIconString = '';
    } else if(batLevel > 0) {
        batIconString = '';
    }

    let today = evt.date;
    let monthString = today.getMonth() + 1;
    let dateString = today.getDate();
    let dayString = dayArray[today.getDay()];

    let hours = today.getHours();
    let ampmString = '';
    if (preferences.clockDisplay === '12h') {
        // 12h format
        ampmString = hours > 12 ? 'PM' : 'AM';
        hours = hours % 12 || 12;
    }
    let hourString = `${hours}`;
    let minString = util.zeroPad(today.getMinutes());
    let secString = util.zeroPad(today.getSeconds());

    tickCallback({
        hour: hourString,
        min: minString,
        sec: secString,
        ampm: ampmString,
        month: monthString,
        date: dateString,
        day: dayString,
        batIcon: batIconString,
        batLevel: batLevelString,
    });
}
