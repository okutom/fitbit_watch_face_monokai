import { HeartRateSensor } from 'heart-rate';
import { me } from 'appbit';
import { display } from 'display';

const CALLBACK_BUFFER = 1; // 1s

export function init(callback) {
    if (me.permissions.granted('access_heart_rate')) {
        const hrm = new HeartRateSensor({frequency: CALLBACK_BUFFER});

        hrm.addEventListener('reading', () => {
            callback({bpm: hrm.heartRate});
        });

        display.addEventListener('change', () => {
            if (display.on) {
                hrm.start();
            } else {
                hrm.stop();
            }
        });

        hrm.start();
    } else {
        callback({bpm: '???'});
    }
}

