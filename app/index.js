import document from 'document';
import { FitFont } from './lib/fitfont';
import * as libTick from './lib/tick';
import * as libHeartRate from './lib/heartRate';
import * as libSteps from './lib/steps';

const dateLabel = new FitFont({
    id: 'dateLabel',
    font: 'Meslo',
    halign: 'start',
    valign: 'baseline',
    letterspacing: 0
});

const batIconLabel = new FitFont({
    id: 'batIconLabel',
    font: 'Meslo',
    halign: 'end',
    valign: 'baseline',
    letterspacing: 0
});
const batLevelLabel = new FitFont({
    id: 'batLevelLabel',
    font: 'Meslo',
    halign: 'start',
    valign: 'baseline',
    letterspacing: 0
});

const ampmLabel = new FitFont({
    id: 'ampmLabel',
    font: 'Meslo',
    halign: 'start',
    valign: 'baseline',
    letterspacing: 0
});
const hourLabel = new FitFont({
    id: 'hourLabel',
    font: 'Meslo',
    halign: 'end',
    valign: 'baseline',
    letterspacing: 0
});
const colonLabel = new FitFont({
    id: 'colonLabel',
    font: 'Meslo',
    halign: 'middle',
    valign: 'baseline',
    letterspacing: 0
});
const minLabel = new FitFont({
    id: 'minLabel',
    font: 'Meslo',
    halign: 'start',
    valign: 'baseline',
    letterspacing: 0
});
const secondsLabel = new FitFont({
    id: 'secondsLabel',
    font: 'Meslo',
    halign: 'end',
    valign: 'baseline',
    letterspacing: 0
});

const heartIconLabel = new FitFont({
    id: 'heartIconLabel',
    font: 'Meslo',
    halign: 'end',
    valign: 'baseline',
    letterspacing: 0
});
const heartRateLabel = new FitFont({
    id: 'heartRateLabel',
    font: 'Meslo',
    halign: 'start',
    valign: 'baseline',
    letterspacing: 0
});

const stepsIconLabel = new FitFont({
    id: 'stepsIconLabel',
    font: 'Meslo',
    halign: 'end',
    valign: 'baseline',
    letterspacing: 0
});
const stepsLabel = new FitFont({
    id: 'stepsLabel',
    font: 'Meslo',
    halign: 'start',
    valign: 'baseline',
    letterspacing: 0
});

const granularity = 'seconds';

libTick.init(granularity, function(data) {
    dateLabel.text = data.month + '/' + data.date + '(' + data.day + ')';

    batIconLabel.text = data.batIcon;
    batLevelLabel.text = data.batLevel;

    hourLabel.text = data.hour;
    colonLabel.text = ':';
    minLabel.text = data.min;
    secondsLabel.text = data.sec;
    ampmLabel.text = data.ampm;
});

libSteps.init(function(data) {
    stepsIconLabel.text = '';
    stepsLabel.text = data.steps;
});

libHeartRate.init(function(data) {
    heartIconLabel.text = '';
    heartRateLabel.text = data.bpm;
});
