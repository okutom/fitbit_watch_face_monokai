#!/bin/sh

if [ -e resources/Meslo ]; then
    rm -rf resources/Meslo
fi
font_size=200
curl -L https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/Meslo/S/Regular/complete/Meslo%20LG%20S%20Regular%20Nerd%20Font%20Complete%20Mono.ttf -o meslo.ttf
npx fitfont-generate meslo.ttf $font_size 1234567890%\(\)-./:?AFMPSTWadehinortu
mv resources/MesloLGS_Nerd_Font_Mono_$font_size resources/Meslo
