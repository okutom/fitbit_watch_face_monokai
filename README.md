# Monokai colored watch face for Fitbit Versa 3

## How to setup

```
$ git clone https://gitlab.com/okutom/fitbit_watch_face_monokai.git
$ npm install
$ ./genfont.sh
```

## How to build & debug

launch simulator or connect phone with developper mode. then,
```
$ npx fitbit-build
$ npx fitbit
# build & install to simulator
fitbit$ bi
```
